// Zad 1.1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>
#include <omp.h>
#include <fstream>
using namespace std;
DWORD WINAPI ThreadFunc(LPVOID lpdwParam) {
	return 0;
}

int _tmain(int argc, _TCHAR* argv[])
{
	DWORD dwThreadId;
	fstream plik;
	plik.open("plik.txt");
	HANDLE ahThread;
	int i = 0;
	double start, stop, result, sum = 0;
	while (i<20)
	{
		start = omp_get_wtime();
		ahThread = CreateThread(NULL, 0, ThreadFunc, 0, 0, &dwThreadId);
		WaitForSingleObject(ahThread, INFINITE);
		stop = omp_get_wtime();
		result = stop - start;
		sum += result;
		i += 1;
		printf("Czas utworzenia i zwolnienia watku %d: %f\n", i, result);
		plik << result<<endl;
	}
	plik.close();
	CloseHandle(ahThread);
	printf("Sredni czas utworzenia i zwolnienia watku: %f\n", sum / 20);
	system("pause");
	return 0;
}